CC=gcc

CFLAGS=-Wall -Wextra -Werror -O0 -g -std=c99 -pthread
LDFLAGS=-lm

all: grade gradet

aluno: roteamentot.c aluno.c
	$(CC) $(CFLAGS) $(LDFLAGS) -o aluno roteamentot.c aluno.c

test: roteamento.c test.c
	$(CC) $(CFLAGS) $(LDFLAGS) -o test roteamento.c test.c

testt: roteamentot.c test.c
	$(CC) $(CFLAGS) $(LDFLAGS) -o testt roteamentot.c test.c


grade: test
	./test

gradet: testt
	./testt

teste_aluno: aluno
	./aluno

clean:
	rm -rf *.o test testt aluno
