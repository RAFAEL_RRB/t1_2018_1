#include "roteador.h"
#include <stdio.h>
#include <stdlib.h>

/* Função que totaliza o número de pacotes encaminhados para cada enlace 
 * @param rotas lista de rotas (um vetor)
 * @param num_rotas número de elementos da lista de rotas
 * @param pacotes lista de endereços de destino dos pacotes a serem encaminhados (um vetor)
 * @param num_pacotes número de pacotes a serem encaminhados
 * @param filtros lista de endereços de destino a serem filtrados
 * @param num_filtros número de filtros
 * @param num_enlaces número de enlaces de saída
 * @return vetor com número de pacotes encaminhados por enlace, com primeira posição sendo filtrados e descartados
 */
uint32_t * roteamento(entrada * rotas, int num_rotas, uint32_t * pacotes, int num_pacotes, entrada * filtros, int num_filtros, int num_enlaces){

	entrada *vrotas = rotas;
	entrada *vfiltros = filtros; 
	uint32_t *vpacotes = pacotes;

	uint32_t *venlaces = (uint32_t *) malloc(num_enlaces+2);


	entrada aux; 
	uint32_t paux,raux;
	int i, j,k, achou=0, achouf=0; 
 		
	for(i=0;i<num_rotas+1;i++){
		venlaces[i]=0;
	}


 		//Ordena o vetor de rotas 
		for(i=0; i<num_rotas; i++){ 
			for(j=i+1; j<num_rotas; j++){ 
				if(vrotas[i].mascara < vrotas[j].mascara){ 
					aux=vrotas[i]; 
		        	vrotas[i]=vrotas[j]; 
		        	vrotas[j]=aux; 
		    	}
			} 
		} 
		//comparando a rota com os pacotes
		for(i=0;i< num_pacotes;i++){

			k=0;
			while(k<num_filtros && achouf!=1){

				paux=vpacotes[i]>>(32-vfiltros[k].mascara);
				raux=vfiltros[k].endereco>>(32-vfiltros[k].mascara);

				if(paux==raux){//ve se a filtro e o pacote são iguais
					venlaces[0]++;
					k=0;
					achouf=1;
					achou=1;
				}
				else{
					k++;
				}
			}

			j=0;
			while(j< num_rotas && achou!=1){

				paux=vpacotes[i]>>(32-vrotas[j].mascara);
				raux=vrotas[j].endereco>>(32-vrotas[j].mascara);

				if(paux==raux){//ve se a rota e o pacote são iguais
					venlaces[(int)vrotas[j].enlace]=venlaces[(int)vrotas[j].enlace]+1;
					achou=1;
				}
			j++;
			}
			if(achou==0){ // caso não tenha rota para o pacote
				venlaces[0]++;
			}
			achouf=0;
			achou=0;
		}

	return venlaces;

}