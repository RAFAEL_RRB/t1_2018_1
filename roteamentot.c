#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include "roteador.h"
/* Função que totaliza o número de pacotes encaminhados para cada enlace 
 * @param rotas lista de rotas (um vetor)
 * @param num_rotas número de elementos da lista de rotas
 * @param pacotes lista de endereços de destino dos pacotes a serem encaminhados (um vetor)
 * @param num_pacotes número de pacotes a serem encaminhados
 * @param filtros lista de endereços de destino a serem filtrados
 * @param num_filtros número de filtros
 * @param num_enlaces número de enlaces de saída
 * @return vetor com número de pacotes encaminhados por enlace, com primeira posição sendo filtrados e descartados
 */

 #define N 1000

// Semaforo para acesso a variavel compartilhada
pthread_mutex_t controle_mutex;

uint32_t buf[N]; // buffer


/* Estrutura de dados para o Buffer */
typedef struct {
    size_t numit; // número de itens no buffer

	entrada * brotas;
	int bnum_rotas;
	uint32_t *bpacotes;
	int bnum_pacotes;
	entrada * bfiltros;
	int bnum_filtros;
	int bnum_enlaces;

	uint32_t *venlaces;
} buffer_c;

void * produtor(void *prod){
	buffer_c *buff = (buffer_c *) prod;

	for(int i=0;i < buff->bnum_pacotes;i++){
		//------------------seção critica-----------
		pthread_mutex_lock(&controle_mutex);
		buf[i]=buff->bpacotes[i];
		pthread_mutex_unlock(&controle_mutex);
		//------------------------------------------
	}
	return 0;
}

void * comsumidor(void *cons){
	buffer_c *buff = (buffer_c *) cons;

	
	entrada *vrotas = buff->brotas;
	entrada *vfiltros = buff->bfiltros;



	entrada aux; 
	uint32_t paux,raux;
	int i , j , k , achou=0 , achouf=0; 
 		
	for(i=0;i<buff->bnum_rotas+1;i++){
		buff->venlaces[i]=0;
	}


 		//Ordena o vetor de rotas 
		for(i=0; i<buff->bnum_rotas; i++){ 
			for(j=i+1; j<buff->bnum_rotas; j++){ 
				if(vrotas[i].mascara < vrotas[j].mascara){ 
					aux=vrotas[i]; 
		        	vrotas[i]=vrotas[j]; 
		        	vrotas[j]=aux; 
		    	}
			} 
		} 
		//comparando a rota com os pacotes
		for(i=0;i< buff->bnum_pacotes;i++){

			k=0;
			while(k<buff->bnum_filtros && achouf!=1){

				//------------------seção critica-----------
				pthread_mutex_lock(&controle_mutex);

				paux=buf[i]>>(32-vfiltros[k].mascara);
				raux=vfiltros[k].endereco>>(32-vfiltros[k].mascara);

				pthread_mutex_unlock(&controle_mutex);
				//------------------------------------------

				if(paux==raux){//ve se a filtro e o pacote são iguais
					buff->venlaces[0]++;
					k=0;
					achouf=1;
					achou=1;
				}
				else{
					k++;
				}
			}




			j=0;
			while(j< buff->bnum_rotas && achou!=1){

				//------------------seção critica-----------
				pthread_mutex_lock(&controle_mutex);

				paux=buf[i]>>(32-vrotas[j].mascara);
				raux=vrotas[j].endereco>>(32-vrotas[j].mascara);

				pthread_mutex_unlock(&controle_mutex);
				//------------------------------------------

				if(paux==raux){//ve se a rota e o pacote são iguais
					buff->venlaces[(int)vrotas[j].enlace]=buff->venlaces[(int)vrotas[j].enlace]+1;
					achou=1;
				}
			j++;
			}
			if(achou==0){ // caso não tenha rota para o pacote
				buff->venlaces[0]++;
			}
			achouf=0;
			achou=0;
		}
		return 0;
}

uint32_t * roteamento(entrada * rotas, int num_rotas, uint32_t * pacotes, int num_pacotes, entrada * filtros, int num_filtros, int num_enlaces){

	pthread_t prod, consu[4];

    int i=0;

	buffer_c buffer = {
		.numit = 0,

		.brotas=rotas,
		.bnum_rotas=num_rotas,
		.bpacotes=pacotes,
		.bnum_pacotes=num_pacotes,
		.bfiltros=filtros,
		.bnum_filtros=num_filtros,
		.bnum_enlaces=num_enlaces,
		.venlaces = (uint32_t *) malloc(num_enlaces+1)

	};

	
	//criando mutex pra controlar secão critica
	pthread_mutex_init(&controle_mutex, NULL);

	// assim não funciona
	pthread_create(&prod, NULL, produtor, (void *)&buffer);

	pthread_join(prod, NULL);

	for(i=0;i<4;i++){
	pthread_create(&consu[i], NULL, comsumidor, (void *)&buffer);
	pthread_join(consu[i], NULL);
	}
	pthread_mutex_destroy(&controle_mutex);


	return buffer.venlaces;
}
