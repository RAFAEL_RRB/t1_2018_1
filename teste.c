#include "roteador.h"
#include "simpletest.h"
#include <assert.h>


uint32_t iptoint(int a, int b, int c, int d){
	return (uint32_t) (a<<24)+(b<<16)+(c<<8)+d;
}




void test1(){
	entrada rotas[4] = { iptoint(5,4,0,0), 16, 1,
						 iptoint (99,0,0,0), 8, 2,
						 iptoint(123,0,0,0), 24, 3,
						 iptoint(200,0,1,0), 24, 4 };

	uint32_t pacs[4] = { iptoint(-5,4,0,1), iptoint(-99,0,0,1), iptoint(-123,0,0,1), iptoint(-200,0,1,1) };
	uint32_t * result;

	WHEN("Tenho 4 rotas disjuntas");
	IF("Envio 4 pacote negativos");

	result = roteamento(rotas, 4, pacs, 4,0,0, 4);
	assert(result);
	THEN("Devo ter descartado 4 pacotes");
	isEqual(result[0], 4, 1); 
	THEN("Não deve ter pacotes nos enlaces");
	for(int i=1; i<=4; i++){
		isEqual(result[i], 0, 1);
	}

}


void test2(){
	entrada rotas[2] = { iptoint(5,4,0,0), 0, 1,
						 iptoint(5,4,1,0), 32, 2 };

	uint32_t pacs[2] = { iptoint(5,4,0,1), iptoint(5,4,1,1) };
	uint32_t * result;

	WHEN("Tenho duas rotas com prefixos iguais mas os valores da mascara em 0 e 32");
	IF("Não deve enviar pacote pra nem uma rota");
	THEN("Deve ter descasrtado as 2 ");

	result = roteamento(rotas, 2, pacs, 2,0,0, 2);
	assert(result);

	isEqual(result[0], 2, 1); 
	for(int i=1; i<=2; i++){
		isEqual(result[i], 0, 1);
	}

}

void test3(){
	entrada rotas[1] = { iptoint(5,4,0,0), 16, 1 };

	uint32_t pacs[1] = { iptoint(5,4,0,1) };

	entrada filt[1] = { iptoint(5,4,0,0), 16, 0 };

	uint32_t * result;

	WHEN("Tenho 1 rota e 1 filtro igual");
	IF("Envio 1 pacote que deve ser pego pelo filtro");

	result = roteamento(rotas, 1, pacs, 1,filt,1, 1);
	assert(result);
	THEN("Devo ter descartado 1 pacote");
	isEqual(result[0], 1, 1); 
	THEN("Não deve ter pacotes nos enlaces");
	for(int i=1; i<=1; i++){
		isEqual(result[i], 0, 1);
	}

}









int main(int argc, char ** argv){


	DESCRIBE("Testes do simulador de roteador CIDR");


	test1();
	test2();
	test3();


	GRADEME();

	if (grade==maxgrade)
		return 0;
	else return grade;

}
